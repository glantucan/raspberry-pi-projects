import spidev # SPI Python library
import time # delay

spi = spidev.SpiDev()
spi.open(0,0) # SPI0
spi.mode = 0
spi.max_speed_hz = 8000000
HEART = (
    0b00000000,
    0b01100110,
    0b11111111,
    0b11111111,
    0b11111111,
    0b01111110,
    0b00111100,
    0b00011000
)


images = ( # R      B          G
    ((HEART, 16),(HEART, 0), (HEART, 0)),
    ((HEART, 16),(HEART, 0), (HEART, 1)),
    ((HEART, 16),(HEART, 0), (HEART, 2)),
    ((HEART, 16),(HEART, 0), (HEART, 3)),
    ((HEART, 16),(HEART, 0), (HEART, 4)),
    ((HEART, 16),(HEART, 0), (HEART, 5)),
    ((HEART, 16),(HEART, 0), (HEART, 6)),
    ((HEART, 16),(HEART, 0), (HEART, 7)),
    ((HEART, 16),(HEART, 0), (HEART, 8)),
    ((HEART, 16),(HEART, 0), (HEART, 9)),
    ((HEART, 16),(HEART, 0), (HEART, 10)),
    ((HEART, 16),(HEART, 0), (HEART, 11)),
    ((HEART, 16),(HEART, 0), (HEART, 12)),
    ((HEART, 16),(HEART, 0), (HEART, 13)),
    ((HEART, 16),(HEART, 0), (HEART, 14)),
    ((HEART, 16),(HEART, 0), (HEART, 15)),
    ((HEART, 16),(HEART, 0), (HEART, 16)),
    ((HEART, 15),(HEART, 0), (HEART, 16)),
    ((HEART, 14),(HEART, 0), (HEART, 16)),
    ((HEART, 13),(HEART, 0), (HEART, 16)),
    ((HEART, 12),(HEART, 0), (HEART, 16)),
    ((HEART, 11),(HEART, 0), (HEART, 16)),
    ((HEART, 10),(HEART, 0), (HEART, 16)),
    ((HEART, 9),(HEART, 0), (HEART, 16)),
    ((HEART, 8),(HEART, 0), (HEART, 16)),
    ((HEART, 7),(HEART, 0), (HEART, 16)),
    ((HEART, 6),(HEART, 0), (HEART, 16)),
    ((HEART, 5),(HEART, 0), (HEART, 16)),
    ((HEART, 4),(HEART, 0), (HEART, 16)),
    ((HEART, 3),(HEART, 0), (HEART, 16)),
    ((HEART, 2),(HEART, 0), (HEART, 16)),
    ((HEART, 1),(HEART, 0), (HEART, 16)),
    ((HEART, 0),(HEART, 0), (HEART, 16)),
    ((HEART, 0),(HEART, 1), (HEART, 16)),
    ((HEART, 0),(HEART, 2), (HEART, 16)),
    ((HEART, 0),(HEART, 3), (HEART, 16)),
    ((HEART, 0),(HEART, 4), (HEART, 16)),
    ((HEART, 0),(HEART, 5), (HEART, 16)),
    ((HEART, 0),(HEART, 6), (HEART, 16)),
    ((HEART, 0),(HEART, 7), (HEART, 16)),
    ((HEART, 0),(HEART, 8), (HEART, 16)),
    ((HEART, 0),(HEART, 9), (HEART, 16)),
    ((HEART, 0),(HEART, 10), (HEART, 16)),
    ((HEART, 0),(HEART, 11), (HEART, 16)),
    ((HEART, 0),(HEART, 12), (HEART, 16)),
    ((HEART, 0),(HEART, 13), (HEART, 16)),
    ((HEART, 0),(HEART, 14), (HEART, 16)),
    ((HEART, 0),(HEART, 15), (HEART, 16)),
    ((HEART, 0),(HEART, 16), (HEART, 16)),
    ((HEART, 0),(HEART, 16), (HEART, 15)),
    ((HEART, 0),(HEART, 16), (HEART, 14)),
    ((HEART, 0),(HEART, 16), (HEART, 13)),
    ((HEART, 0),(HEART, 16), (HEART, 12)),
    ((HEART, 0),(HEART, 16), (HEART, 11)),
    ((HEART, 0),(HEART, 16), (HEART, 10)),
    ((HEART, 0),(HEART, 16), (HEART, 9)),
    ((HEART, 0),(HEART, 16), (HEART, 8)),
    ((HEART, 0),(HEART, 16), (HEART, 7)),
    ((HEART, 0),(HEART, 16), (HEART, 6)),
    ((HEART, 0),(HEART, 16), (HEART, 5)),
    ((HEART, 0),(HEART, 16), (HEART, 4)),
    ((HEART, 0),(HEART, 16), (HEART, 3)),
    ((HEART, 0),(HEART, 16), (HEART, 2)),
    ((HEART, 0),(HEART, 16), (HEART, 1)),
    ((HEART, 0),(HEART, 16), (HEART, 0)),
    ((HEART, 1),(HEART, 16), (HEART, 0)),
    ((HEART, 2),(HEART, 16), (HEART, 0)),
    ((HEART, 3),(HEART, 16), (HEART, 0)),
    ((HEART, 4),(HEART, 16), (HEART, 0)),
    ((HEART, 5),(HEART, 16), (HEART, 0)),
    ((HEART, 6),(HEART, 16), (HEART, 0)),
    ((HEART, 7),(HEART, 16), (HEART, 0)),
    ((HEART, 8),(HEART, 16), (HEART, 0)),
    ((HEART, 9),(HEART, 16), (HEART, 0)),
    ((HEART, 10),(HEART, 16), (HEART, 0)),
    ((HEART, 11),(HEART, 16), (HEART, 0)),
    ((HEART, 12),(HEART, 16), (HEART, 0)),
    ((HEART, 13),(HEART, 16), (HEART, 0)),
    ((HEART, 14),(HEART, 16), (HEART, 0)),
    ((HEART, 15),(HEART, 16), (HEART, 0)),
    ((HEART, 16),(HEART, 16), (HEART, 0)),
    ((HEART, 16),(HEART, 15), (HEART, 0)),
    ((HEART, 16),(HEART, 14), (HEART, 0)),
    ((HEART, 16),(HEART, 13), (HEART, 0)),
    ((HEART, 16),(HEART, 12), (HEART, 0)),
    ((HEART, 16),(HEART, 11), (HEART, 0)),
    ((HEART, 16),(HEART, 10), (HEART, 0)),
    ((HEART, 16),(HEART, 9), (HEART, 0)),
    ((HEART, 16),(HEART, 8), (HEART, 0)),
    ((HEART, 16),(HEART, 7), (HEART, 0)),
    ((HEART, 16),(HEART, 6), (HEART, 0)),
    ((HEART, 16),(HEART, 5), (HEART, 0)),
    ((HEART, 16),(HEART, 4), (HEART, 0)),
    ((HEART, 16),(HEART, 3), (HEART, 0)),
    ((HEART, 16),(HEART, 2), (HEART, 0)),
    ((HEART, 16),(HEART, 1), (HEART, 0))
)

pulse_width = 12
pulse_slice_count = 0
img_idx = 12
frame = 0
try:
    while True:
        if frame == 50:
            img_idx += 1
            if img_idx == len(images):
                img_idx = 0
            frame = 0
        frame += 1
        image = images[img_idx] 
        
        # update screen
        for row in range(0, 8):
            data = [0xFF, 0xFF, 0xFF, 0x01 << row]
            for color in range(0,3):
                # Take the values of the row for the color corresponding to this PWM slice
                if pulse_slice_count % 3 == color :
                    color_dc = image[color][1]
                    # But only if the color value is higher than the currrent slice
                    # Note that we are calling ig it _dc for duty_cycle but only one color is
                    # lit each matrix scan
                    if int(pulse_slice_count/3) < color_dc :
                        data[color] = ~image[color][0][row]
                    #print( f"{row} {color} {color_dc} {~data[color]:08b}")           
            # We send data once per row
            # Each time we send data the previous row gets erased
            spi.xfer(data)
            

            # This is only necesary if we are not sending data fast enoyugh (the last row stays lit for longer time)
            """ data = [0xff, 0xff, 0xff, 0x00]
            spi.xfer(data)  """
        # After we have drawn a complete screen color we wait a small time
        # A complete PWM colored screen refresh will take 3 * 8 * 16 * sleep time
        time.sleep(0.00001)
        pulse_slice_count += 1
        if pulse_slice_count >= pulse_width * 3 : pulse_slice_count = 0
        #time.sleep(0.1)
        



except KeyboardInterrupt:
    print("\nGot tired... cleaning up and finishing!")
    data = [0x00, 0x00, 0x00, 0x00]  # turning off the LED matrix.
    spi.xfer(data)
