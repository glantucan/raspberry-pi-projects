import spidev # SPI Python library
import time # delay

spi = spidev.SpiDev()
spi.open(0,0) # SPI0
spi.mode = 0
spi.max_speed_hz = 500000 # By changing this to a lower value, I could notice some red leds light up too.

# The numbers are the hexadecimal representation of 8 bits binary numbers (2^8 = FF)
# Each bit corresponds to a led in the row. Alternating leds would be 0b10101010 = OxAA for example
# You can write the array using binary instead of hexadecimal
# Each number represents the state of a row
HEART = [0b00000000, 0b01100110, 0b11111111, 0b11111111, 0b11111111,  0b01111110, 0b00111100, 0b00011000]
BLANK = [0X00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]

image = [
    [HEART, 100],
    [HEART, 1],
    [HEART, 100]
]
"""
Different color intensities don't work. Probably because alternating pulses of different
durations is not exactly pwm
"""
color_correct = (1,1,1)

fps = 30
frame_t = 1 / fps   # 0.02s # 100 duty cycles
row_t = frame_t / 10  # 0.002s
color_t = row_t / 60 # 0.000333333s

try:
    while True:

        row_acc = 0
        # update screen
        for row in range(0, 8):
            # update row
            color_acc = 0

            cur_row = 0x01 << row
            for color in range(0,3):
                data = [0xFF, 0xFF, 0xFF, cur_row]
                duty = max(int(image[color][1] // color_correct[color]), 0)
                if duty > 0:
                    data[color] = ~image[color][0][row]
                spi.xfer(data)
                time.sleep(color_t * duty / 100)
                if(duty < 100):
                    time.sleep((color_t - color_t * duty / 100))

                data = [0xFF, 0xFF, 0xFF, cur_row]
                spi.xfer(data)


        #time.sleep(0)


except KeyboardInterrupt:
    print("\nGot tired... cleaning up and finishing!")
    data = [0x00, 0x00, 0x00, 0x00]  # turning off the LED matrix.
    spi.xfer(data)