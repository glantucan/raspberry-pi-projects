import spidev 
import time 

"""
Raspberry Pi pins to use (Except MISO which make no sense in the case of 
non-touch sensitive screens)

Function  Header Pin  Broadcom Pin Name 	Broadcom Pin Function
  MOSI        19 	       GPIO10 	            SPI0_MOSI
  MISO        21 	       GPIO09 	            SPI0_MISO
  SCLK        23 	       GPIO11 	            SPI0_SCLK
  CE0         24 	       GPIO08 	            SPI0_CE0_N
  CE1         26 	       GPIO07 	            SPI0_CE1_N

SPI interface must be activated on the RPI: Add:
    device_tree=bcm2711-rpi-4-b.dtb
    dtparam=spi=on
to your /boot/config.txt.

Each Matrix receives data through the common MOSI pin in sync with the 
SCLK clock signal. One is on the channel 0 (CE0) of the SPI0 interface 
and the other is on the channel 1 (CE1)

If the harware allowed we could daisy chain the two matrices and send 16bit 
numbers instead of 8 for each led row. Didn't test this because these matrices 
don't have output pins.

Another option is activate the second SPI interface on the Raspberry PI 
(Only possible on the 40 pin vesrsions) But that would be an unnecessary 
waste of pins.
"""
spi0 = spidev.SpiDev(0,0)
spi1 = spidev.SpiDev(0,1)

spi0.max_speed_hz = 3200000
spi1.max_speed_hz = 3200000

image = (
    (0x000, 0x000, 0x40C, 0x00E, 0x04C, 0x088, 0x000, 0x000),
    (0x000, 0x808, 0x40C, 0x00E, 0x04C, 0x088, 0x4C4, 0x000),
    (0xC04, 0x808, 0x000, 0x00E, 0x04C, 0x000, 0x4C4, 0x880),
    (0xC04, 0x808, 0x40C, 0x00E, 0x04C, 0x088, 0x4C4, 0x880),
    (0xC04, 0x808, 0x40C, 0x00E, 0x04C, 0x088, 0x4C4, 0x880),
    (0xC04, 0x808, 0x000, 0x00E, 0x04C, 0x088, 0x4C4, 0x880),
    (0x000, 0x808, 0x40C, 0x000, 0x000, 0x088, 0x4C4, 0x000),
    (0x000, 0x000, 0x40C, 0x00E, 0x04C, 0x088, 0x000, 0x000),

    (0x000, 0x000, 0x40C, 0x00E, 0x04C, 0x088, 0x000, 0x000),
    (0x000, 0x808, 0x40C, 0x000, 0x000, 0x088, 0x4C4, 0x000),
    (0xC04, 0x808, 0x000, 0x00E, 0x04C, 0x088, 0x4C4, 0x880),
    (0xC04, 0x808, 0x40C, 0x00E, 0x04C, 0x088, 0x4C4, 0x880),
    (0xC04, 0x808, 0x40C, 0x00E, 0x04C, 0x088, 0x4C4, 0x880),
    (0xC04, 0x808, 0x000, 0x00E, 0x04C, 0x000, 0x4C4, 0x880),
    (0x000, 0x808, 0x40C, 0x00E, 0x04C, 0x088, 0x4C4, 0x000),
    (0x000, 0x000, 0x40C, 0x00E, 0x04C, 0x088, 0x000, 0x000),

)



"""Returns an 8bit binary representation of the row pixels of a 
specific color to be activated"""
def get_color_row_slice(row, color, slice):
    row_val = 0
    for pix in range(0, 8):
        intensity = (row[pix]//(16**(2-color)))%16 
        if intensity > slice:
            row_val += 2**pix
                       
    return row_val 



pulse_width = 16
pulse_slice_count = 0


try:
    while True:
        
        # update screen
        for row in range(0, 8):
            s0_data = [0xFF, 0xFF, 0xFF, 0x01 << row]
            s1_data = [0xFF, 0xFF, 0xFF, 0x01 << row]
            for color in range(0,3): # 0: Red, 1:Blue, 2: Green
                # Only light one color at a time on each row
                if pulse_slice_count % 3 == color:
                    # Calculate the values of the row for the color corresponding to this PWM slice
                    # Leds get activated when low so we need to send the binary complement
                    s0_data[color] = ~get_color_row_slice(image[row], color, pulse_slice_count)  
                    s1_data[color] = ~get_color_row_slice(image[row + 8], color, pulse_slice_count)
                    #print([f"0x{image[row][pix]:03x}" for pix in range(0,8)], f"{color} {pulse_slice_count}", f"{get_color_row_slice(image[row], color, pulse_slice_count):#010b}")
            # We send data once per row
            # Each time we send data the previous row gets erased
            
            spi0.xfer(s0_data)
            spi1.xfer(s1_data)

            # If we wait too much between frames we need to ensure the last row gets erased
            # otherwise it will remain lit until the start of the next redraw
            """ data = [0xff, 0xff, 0xff, 0x00]
            spi.xfer(data)  """

        # After we have drawn a complete screen color we wait a small time
        # A complete PWM colored screen refresh will take 3 * 16 * sleep time
        time.sleep(0.000001)
        pulse_slice_count += 1
        if pulse_slice_count >= pulse_width : pulse_slice_count = 0
        #time.sleep(0.1)
        
except KeyboardInterrupt:
    print("\nGot tired... cleaning up and finishing!")
    data = [0x00, 0x00, 0x00, 0x00]  # turning off the LED matrix.
    spi0.xfer(data)
    spi1.xfer(data)
    spi0.close()
    spi1.close()
