import spidev # SPI Python library
import time # delay

spi = spidev.SpiDev()
spi.open(0,0) # SPI0
spi.mode = 0
spi.max_speed_hz = 500000 # By changing this to a lower value, I could notice some red leds light up too.

# These numbers are the hexadecimal representation of 8 bits binary numbers (2^8 = FF)
# Each bit corresponds to a led in the row. Alternating leds would be 0b10101010 = OxAA for example
# You can write the array using binary instead of hexadecimal
HEART = [0b00000000, 0b01100110, 0b11111111, 0b11111111, 0b11111111,  0b01111110, 0b00111100, 0b00011000]
#HEART = [0x00, 0x66, 0xFF, 0xFF, 0xFF, 0x7E, 0x3C, 0x18] # heart array
EMPTY = [0X00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
R = [0X00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
G = [0X00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
B = [0X00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]

data = [0x00, 0x00, 0x00, 0x00] # SPI array. Data is changed in the for loop.
pwm_counter = 0

if __name__ == '__main__':
    try:
        while True:
            if pwm_counter % 7 == 0:
                G = HEART
                R = EMPTY
            else:
                R = HEART
                G = EMPTY

            for i in range(0, 8):
                data[0] = ~R[i]  # RED - LEDs should turn on
                data[1] = ~B[i]  # BLUE  - LEDs should turn on
                data[2] = ~G[i]  # GREEN
                data[3] = 0x01 << i  # turns on each row
                spi.xfer(data)  # send data to LED Matrix
                time.sleep(0.1)  # multiplexing effect is working when using this delay value.
            pwm_counter += 1

    except KeyboardInterrupt:
        print("\nGot tired... cleaning up and finishing!")
        data = [0x00, 0x00, 0x00, 0x00] # turning off the LED matrix.
        spi.xfer(data)
