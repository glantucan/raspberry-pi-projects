import spidev # SPI Python library
import time # delay

spi = spidev.SpiDev()
spi.open(0,0) # SPI0
spi.mode = 0
spi.max_speed_hz = 500000 # By changing this to a lower value, I could notice some red leds light up too.

# These numbers are the hexadecimal representation of 8 bits binary numbers (2^8 = FF)
# Each bit corresponds to a led in the row. Alternating leds would be 0b10101010 = OxAA for example
# You can write the array using binary instead of hexadecimal
HEART = [0b00000000, 0b01100110, 0b11111111, 0b11111111, 0b11111111,  0b01111110, 0b00111100, 0b00011000]
#HEART = [0x00, 0x66, 0xFF, 0xFF, 0xFF, 0x7E, 0x3C, 0x18] # heart array
EMPTY = [0X00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
data = [0x00, 0x00, 0x00, 0x00] # SPI array. Data is changed in the for loop.

MATRIX = [
    [HEART, EMPTY, EMPTY],  # RED
    [EMPTY, HEART, EMPTY],  # BLUE
    [EMPTY, EMPTY, HEART]   # GREEN
]
# PWM_WEIGHT = [10, 7, 8]
PWM_WEIGHT = [999, 0, 1]
base_delay = 0.0000001
max_delay = 0.0001
if __name__ == '__main__':
    try:
        while True:
            for i in range(0, 8):
                row = 0x01 << i
                acc_delay = 0
                for color in range(0, 3):
                    if(PWM_WEIGHT[color] > 0):
                        data[0] = ~MATRIX[color][0][i]  # RED - LEDs should turn on
                        data[1] = ~MATRIX[color][1][i]  # BLUE  - LEDs should turn on
                        data[2] = ~MATRIX[color][2][i]  # GREEN
                        data[3] = row  # turns on each row
                        spi.xfer(data)  # send data to LED Matrix
                        acc_delay += PWM_WEIGHT[color] * base_delay
                        time.sleep(PWM_WEIGHT[color] * base_delay)  # multiplexing effect is working when using this delay value.
                        #print(pwm, data[0], COLORS[pwm])
                data[0] = ~EMPTY[i]  # RED - LEDs should turn on
                data[1] = ~EMPTY[i]  # BLUE  - LEDs should turn on
                data[2] = ~EMPTY[i]  # GREEN
                data[3] = row  # turns on each row
                spi.xfer(data)  # send data to LED Matrix
                time.sleep(max_delay - acc_delay)


    except KeyboardInterrupt:
        print("\nGot tired... cleaning up and finishing!")
        data = [0x00, 0x00, 0x00, 0x00] # turning off the LED matrix.
        spi.xfer(data)
