import spidev # SPI Python library
import time # delay
spi = spidev.SpiDev()
spi.open(0,0) # SPI0
spi.mode = 0
spi.max_speed_hz = 500000
HEART = (
    0b00000000,
    0b01100110,
    0b11111111,
    0b11111111,
    0b11111111,
    0b01111110,
    0b00111100,
    0b00011000)


BLANK = (0X00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00)

images = ( # R      B          G
    ((HEART, 1),(HEART, 0), (HEART, 0)),
    ((HEART, 2),(HEART, 0), (HEART, 0)),
    ((HEART, 2),(HEART, 0), (HEART, 1)),
    ((HEART, 2),(HEART, 0), (HEART, 2)),
    ((HEART, 1),(HEART, 0), (HEART, 2)),
    ((HEART, 0),(HEART, 0), (HEART, 2)),
    ((HEART, 0),(HEART, 1), (HEART, 2)),
    ((HEART, 0),(HEART, 2), (HEART, 2)),
    ((HEART, 0),(HEART, 2), (HEART, 1)),
    ((HEART, 0),(HEART, 2), (HEART, 0)),
    ((HEART, 1),(HEART, 2), (HEART, 0)),
    ((HEART, 2),(HEART, 2), (HEART, 0)),
    ((HEART, 2),(HEART, 1), (HEART, 0)),
    ((HEART, 1),(HEART, 1), (HEART, 0)),
    ((HEART, 1),(HEART, 1), (HEART, 1)),
    ((HEART, 2),(HEART, 2), (HEART, 2)),
    ((HEART, 1),(HEART, 1), (HEART, 1)),
    ((HEART, 1),(HEART, 0), (HEART, 1)),
    ((HEART, 2),(HEART, 0), (HEART, 0)),
    ((HEART, 1),(HEART, 0), (HEART, 0))
)

fps = 30
frame_t = 1 / fps   # 0.02s # 100 duty cycles
row_t = frame_t / 8 # 0.002s
color_t = row_t / 3
pwm_t = 0.0000001#color_t / 10 # 0.000333333s
img_idx = 1
frame = 0

try:
    while True:
        # Fade colors every 4 frames
        if frame >= fps:
            img_idx = 1
            frame = 0
        elif frame == int(fps/2):
            img_idx = 3
                
                
            """
            img_idx += 1
            if img_idx == len(images):
                img_idx = 0
            """

        image = images[img_idx]
        frame += 1

        # update screen
        for row in range(0, 8):
            # update row
            cur_row = 0x01 << row

            color_counter = [0,0,0]
            for i in range(0, 2):
                for color in range(0,3):
                    data = [0xFF, 0xFF, 0xFF, cur_row]

                    if color_counter[color] < image[color][1]:
                        data[color] = ~image[color][0][row]
                        spi.xfer(data)

                    time.sleep(pwm_t)
                    data = (0xFF, 0xFF, 0xFF, cur_row)
                    spi.xfer(data)
                    color_counter[color] += 1



except KeyboardInterrupt:
    print("\nGot tired... cleaning up and finishing!")
    data = [0x00, 0x00, 0x00, 0x00]  # turning off the LED matrix.
    spi.xfer(data)
