
#include <stdio.h>
#include <wiringPi.h>
#include <wiringPiSPI.h>
#include <stdint.h>

#define RED_DATA 0
#define BLUE_DATA 1
#define GREEN_DATA 2

int main(void)
{
	static uint8_t data[4] = {0x0,0x0,0x0,0x0};

	wiringPiSetup();
	wiringPiSPISetup(0,500000);
	int pwm_count = 0;
	while(1) {
		static uint8_t heart[8] = {0x00, 0x66, 0xFF, 0xFF, 0xFF, 0x7E, 0x3C, 0x18};             // this is a array of heart
		int j;
		float x=.01;
		for ( j=0;j<8;j++) {
			if (pwm_count % 4 == 0 || pwm_count % 4 == 1 || pwm_count % 4 == 2 ) {
				data[0] = ~heart[j];		// RED
				data[2] = 0xFF;				// GREEN
			} else {
				data[0] = 0xFF;		// RED
				data[2] = ~heart[j];				// GREEN
			}
			data[1] = 0xFF;		// BLUE
			data[3] = 0x01 << j ;	// DOT ROW MATRIX SCAN
			wiringPiSPIDataRW(0,data,sizeof(data));              // send data to SPI channel 0, and the length of the data
			delay(x);
		};
		pwm_count++;
	};
}
