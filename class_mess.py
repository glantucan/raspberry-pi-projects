class A:
    a = 10
    def get_a(self):
        return self.a

myA = A()
print(f"A.a is {A.a}")
A.a = 20
print(f"myA.get_a() returns {myA.get_a()}")
print(f"myA.a is {myA.a}")
print(f"A.a is {A.a}")


### Better this way

class B:
    def __init__(self):
        self.b = 10

    def get_b(self):
        return self.b


myB = B()
# print(B.b)  # AttributeError: type object 'B' has no attribute 'b'
B.b = 20  ## This creates a static variable b but doesn't affect the instance variable
print(f"myB.get_b() returns {myB.get_b()}")
print(f"myB.b is {myB.b}")
print(f"B.b is {B.b}")

## Getting weirder
get_b = B.get_b
print(get_b(B))
print(get_b(myB))
print(get_b())