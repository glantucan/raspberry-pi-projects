import threading
import time

class Ticker(threading.Timer):
    def run(self):
        while not self.finished.wait(self.interval):
            self.function(*self.args, **self.kwargs)

prev_time = time.time()
acc_time = 0
game_ticker = None

def game_loop():
    global prev_time
    global acc_time
    global game_ticker
    new_time = time.time()
    acc_time += new_time - prev_time
    print(f"acc: {acc_time}, delta: {new_time - prev_time}")
    if acc_time > 10.0:
        game_ticker.cancel()
    prev_time = new_time

game_ticker = Ticker(1/60, game_loop)
game_ticker.start()

