import RPi.GPIO as GPIO
import time
import random
from pygame import mixer

RED = 18
GREEN = 22
YELLOW = 24
BLUE = 26

COLORS = dict()
COLORS[RED] = 'RED'
COLORS[GREEN] = 'GREEN'
COLORS[YELLOW] = 'YELLOW'
COLORS[BLUE] = 'BLUE'

SOUNDS = dict();


RED_BTN = 32
GREEN_BTN = 36
YELLOW_BTN = 38
BLUE_BTN = 40

lights = [RED, GREEN, YELLOW, BLUE]
pattern = []
pattern_length = 1
cur_input_idx = 0
last_input = None
waiting = False


def hw_setup():
    GPIO.setmode(GPIO.BOARD)
    GPIO.setwarnings(False)
    mixer.pre_init(44100, 16, 2, 4096)    
    mixer.init()
    
    SOUNDS[RED] = mixer.Sound("red.wav")
    SOUNDS[GREEN] = mixer.Sound("green.wav")
    SOUNDS[YELLOW] = mixer.Sound("yellow.wav")
    SOUNDS[BLUE] = mixer.Sound("blue.wav")
    
    
    GPIO.setup(RED, GPIO.OUT)
    GPIO.setup(GREEN, GPIO.OUT)
    GPIO.setup(YELLOW, GPIO.OUT)
    GPIO.setup(BLUE, GPIO.OUT)
    
    GPIO.setup(RED_BTN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(GREEN_BTN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(YELLOW_BTN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(BLUE_BTN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    

def led_on(color):
    GPIO.output(color, GPIO.HIGH)
    
    
def led_off(color):
	GPIO.output(color, GPIO.LOW)
    
def is_pressed(btn):
    return not(GPIO.input(btn))

hw_setup()


def on_btn_color(color):
    def detect_btn(btn):
        global last_input
        if waiting:
            if is_pressed(btn):
                last_input = color
                led_on(color)
                mixer.find_channel().play(SOUNDS[color])
            else:
                led_off(color)
                #SOUNDS[color].stop()
    
    return detect_btn


GPIO.add_event_detect(RED_BTN, GPIO.BOTH, on_btn_color(RED))
GPIO.add_event_detect(GREEN_BTN, GPIO.BOTH, on_btn_color(GREEN))
GPIO.add_event_detect(YELLOW_BTN, GPIO.BOTH, on_btn_color(YELLOW))
GPIO.add_event_detect(BLUE_BTN, GPIO.BOTH, on_btn_color(BLUE))
# GPIO.add_event_detect(RED_BTN, GPIO.BOTH, on_btn_color(RED), bouncetime=10)
# GPIO.add_event_detect(GREEN_BTN, GPIO.BOTH, on_btn_color(GREEN), bouncetime=10)
# GPIO.add_event_detect(YELLOW_BTN, GPIO.BOTH, on_btn_color(YELLOW), bouncetime=10)
# GPIO.add_event_detect(BLUE_BTN, GPIO.BOTH, on_btn_color(BLUE), bouncetime=10)


def play_light(color):
    led_on(color)
    mixer.find_channel().play(SOUNDS[color])
    time.sleep(0.5)
    led_off(color)
    time.sleep(0.05)
    
    
error = mixer.Sound("error.wav")

game = True
pattern.append(lights[random.randint(0,3)])
while game:
    if not waiting:
        print('\nplaying pattern')
        
        cur_input_idx = 0
        for color in pattern:
            play_light(color)
        
        waiting = True
        print('\nwaiting for input...')
    else:
        if last_input:
            print(last_input)

            
            if last_input == pattern[cur_input_idx]:
                cur_input_idx += 1
                if cur_input_idx == len(pattern):
                    rnd_color = lights[random.randint(0,3)]
                    pattern.append(rnd_color)
                    print(f"Added {COLORS[rnd_color]}({rnd_color}) color")
                    cur_input_idx = 0
                    waiting = False
                    time.sleep(0.2)
                    led_off(last_input)
                    time.sleep(1)
                    print(pattern)
                
            else:
                time.sleep(0.5)
                error.play()
                lapse = 0.05
                led_on(BLUE)
                time.sleep(lapse)
                led_off(BLUE)
                led_on(YELLOW)
                time.sleep(lapse)
                led_off(YELLOW)
                led_on(GREEN)
                time.sleep(lapse)
                led_off(GREEN)
                led_on(RED)
                time.sleep(lapse)
                led_off(RED)
                
                time.sleep(1.5)
                waiting = False
                pattern = [];
                pattern.append(lights[random.randint(0,3)])
                cur_input_idx = 0
                
                
            last_input = None

