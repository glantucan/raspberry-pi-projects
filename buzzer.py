import RPi.GPIO as GPIO
import time
import random

buzz1 = 29
pwm1 = None

# def hw_setup():
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
GPIO.setup(buzz1, GPIO.OUT)
GPIO.output(buzz1, True)
pwm1 = GPIO.PWM(buzz1, 160)
pwm1.start(50)

pwm1.stop()
time.sleep(.5)


# TODO: Use threading.Timer to avoid blocking.
# https://stackoverflow.com/questions/15584928/fast-and-precise-python-repeating-timer
# TODO: Alternatively use time.clock() on the game loop to check for time
#  intervals (less memory used)
# hw_setup()
# for f in range(50, 2000, 50):
#     pwm1.ChangeFrequency(f)
#     time.sleep(.25)


try:
    for b in range(0, 100):
        # print(b)
        # pwm1.ChangeFrequency(50)
        # pwm1.start(b)
        # time.sleep(.2)
        # pwm1.stop()
        # time.sleep(.25)
        if b % 4 == 0:
            print("Bombo")
            pwm1.ChangeFrequency(random.randrange(35, 50, 3))
            pwm1.start(99)
            time.sleep(.006)
            for p in range(0, 4):
                pwm1.ChangeFrequency(random.randrange(35, 50, 3))
                time.sleep(.006)

            # print("Bombo")
            # pwm1.ChangeFrequency(40)
            # pwm1.start(99)
            # time.sleep(.03)
            pwm1.stop()
            time.sleep(.247)
        elif b % 2 == 0:
            print("Caja")
            pwm1.ChangeFrequency(random.randrange(70, 120, 10))
            pwm1.start(.001)
            time.sleep(.003)
            for p in range(0, 9):
                pwm1.ChangeFrequency(random.randrange(70, 120, 10))
                time.sleep(.006)
            pwm1.stop()
            time.sleep(.246)
        else:
            print("-")
            time.sleep(.25)
except KeyboardInterrupt:
    print("\nGot tired... cleaning up and finishing!")
    GPIO.cleanup()
