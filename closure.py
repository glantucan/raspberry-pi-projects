def counter_factory(start = 0):
    counter = start
    def count():
        nonlocal counter
        counter += 1
        return counter
    return count


beat_counter = counter_factory(5)
beat_counter()  # 6
print(beat_counter())  # 7