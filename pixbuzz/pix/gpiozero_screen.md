# Screen refactor using gpiozero

## For one matrix

The hardward itself consumes sets of four 16bits numbers through spi
```python
# R     B     G    row
(0x00, 0x00, 0x00, 0x01)
```
The R, G or B numbers represent the respective color distribution of pixels through the row. Representing them as binary numbers makes it easier to understand, because each digit represent a column and a 1 means off and a 0 means on, so
```python
0x03 = 0b00000011 
```
means all the pixels except the last two pixels of the row will be iluminated.

To get several colors I need to send 48=16x3 complete row scans for each screen refresh. **Plus** 16 clean row instructions (0xFF, 0xFF, 0xFF, 0x07) if I'm refreshing in a while loop instead of an independent thread, to avoid the last row to remain lit until the next screen refresh.

My gpiozero value would be a 8x8 tuple representing the pixel colors with 12 bits numbers, in hexadecimal a 3 digit number, each digit representing the intendsity of R, B and G.