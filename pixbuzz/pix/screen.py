
""" Raspberry Pi pins to use (Except MISO which make no sense in the case of
    non-touch sensitive screens)

    Function  Header Pin  Broadcom Pin Name 	Broadcom Pin Function
    MOSI        19 	       GPIO10 	            SPI0_MOSI
    MISO        21 	       GPIO09 	            SPI0_MISO
    SCLK        23 	       GPIO11 	            SPI0_SCLK
    CE0         24 	       GPIO08 	            SPI0_CE0_N
    CE1         26 	       GPIO07 	            SPI0_CE1_N

    SPI interface must be activated on the RPI: Add:
        device_tree=bcm2711-rpi-4-b.dtb
        dtparam=spi=on
    to your /boot/config.txt.

    Each Matrix receives data through the common MOSI pin in sync with the
    SCLK clock signal. One is on the channel 0 (CE0) of the SPI0 interface
    and the other is on the channel 1 (CE1)

    If the harware allowed we could daisy chain the two matrices and send 16bit
    numbers instead of 8 for each led row. Didn't test this because these
    matrices don't have output pins.

    Another option is activate the second SPI interface on the Raspberry PI
    (Only possible on the 40 pin vesrsions) But that would be an unnecessary
    waste of pins.
"""
import spidev

_pulse_width = 16
_pulse_slice_count = 0
_empty_screen = ()
_screen = ()
_spi0 = None
_spi1 = None
_spi_freq = 3500000


def initialize():
    global _screen, _empty_screen, _spi0, _spi1
    _spi0 = spidev.SpiDev(0, 0)
    _spi1 = spidev.SpiDev(0, 1)
    _spi0.max_speed_hz = _spi1.max_speed_hz = _spi_freq
    """ Each number is a three digits color for each pixel cell
        Each color gets 16 bits pulses_per_duty_cycle which is translated to
        PWM (multiplexed, because the matrices can't light several
        LEDs at the same time with reliable pulses_per_duty_cycle, resistance
        accumulates along the row and that results in less brightness
        the more LEDS you light you in  a row) """
    _empty_screen = (
        (0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000),
        (0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000),
        (0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000),
        (0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000),
        (0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000),
        (0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000),
        (0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000),
        (0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000),
        (0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000),
        (0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000),
        (0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000),
        (0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000),
        (0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000),
        (0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000),
        (0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000),
        (0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000),
    )
    _screen = _empty_screen


def _get_color_row_slice(row, color, cur_slice):
    """ Returns an 8bit binary representation of the row pixels of a
    specific color to be activated in the current time slice"""
    row_val = 0
    # Scan the row colors to get the 8 bits number representing which Leds
    # of the current color should be lit
    for pix in range(0, 8):
        cur_color_intensity = row[pix] // (16 ** (2 - color))
        pulses_per_duty_cycle = cur_color_intensity % 16
        if pulses_per_duty_cycle > cur_slice:
            row_val += 2**pix

    return row_val
    # Still sending several the more ones you send on one row the dimmer
    # they will get lit. We could try to redistribute pulses for the same color
    # led on different cells throgh the duty cycle so we send only one (or as
    # little as possible) on each slice. Or multiply by 8 the number of slices
    # and only send one pixel at a time.


def update():
    for row in range(0, 8):
        s0_data = [0xFF, 0xFF, 0xFF, 0x01 << row]
        s1_data = [0xFF, 0xFF, 0xFF, 0x01 << row]
        for color in range(0, 3):  # 0: Red, 1:Blue, 2: Green
            # Only light one color at a time on each row
            global _pulse_slice_count
            if _pulse_slice_count % 3 == color:
                # Calculate the values of the row for the color corresponding
                # to this PWM slice
                # Leds get activated when low so we need to send the binary
                # complement
                s0_data[color] = ~_get_color_row_slice(_screen[row], color,
                                                       _pulse_slice_count)
                s1_data[color] = ~_get_color_row_slice(_screen[row + 8], color,
                                                       _pulse_slice_count)
        # We send data once per row
        # Each time we send data the previous row gets erased
        _spi0.xfer(s0_data)
        _spi1.xfer(s1_data)

    # If we wait too much between frames we need to ensure the last row gets
    # erased otherwise it will remain lit until the start of the next redraw
    """ _spi0.xfer((0xFF, 0xFF, 0xFF, 0x01 << 7))  # turning off the LED matrix.
    _spi1.xfer((0xFF, 0xFF, 0xFF, 0x01 << 7)) """

    _pulse_slice_count += 1
    if _pulse_slice_count >= _pulse_width:
        _pulse_slice_count = 0


def set_image(data):
    global _screen
    _screen = tuple(data)


def set_pixel(x, y, color):
    global _screen
    screen = [list(row) for row in _screen]
    screen[x][y] = color
    _screen = tuple(screen)


def clear():
    global _screen
    _screen = _empty_screen


def turn_off():
    print("Turning screen off")
    data = [0x00, 0x00, 0x00, 0x00]  # turning off the LED matrix.
    _spi0.xfer(data)
    _spi1.xfer(data)
    _spi0.close()
    _spi1.close()


# For testing

if __name__ == '__main__':
    try:
        import time
        image = (
            (0x000, 0x000, 0x40C, 0x00E, 0x04C, 0x088, 0x000, 0x000),
            (0x000, 0x808, 0x40C, 0x00E, 0x04C, 0x088, 0x4C4, 0x000),
            (0xC04, 0x808, 0x000, 0x00E, 0x04C, 0x000, 0x4C4, 0x880),
            (0xC04, 0x808, 0x40C, 0x00E, 0x04C, 0x088, 0x4C4, 0x880),
            (0xC04, 0x808, 0x40C, 0x00E, 0x04C, 0x088, 0x4C4, 0x880),
            (0xC04, 0x808, 0x000, 0x00E, 0x0FF, 0x088, 0x4C4, 0x880),
            (0x000, 0x808, 0x40C, 0x000, 0x000, 0x088, 0x4C4, 0x000),
            (0x000, 0x000, 0x40C, 0x00E, 0x04C, 0x088, 0x000, 0x000),

            (0x000, 0x000, 0x40C, 0x00E, 0x04C, 0x088, 0x000, 0x000),
            (0x000, 0x808, 0x40C, 0x000, 0x000, 0x088, 0x4C4, 0x000),
            (0xC04, 0x808, 0x000, 0x00E, 0x04C, 0x088, 0x4C4, 0x880),
            (0xC04, 0x808, 0x40C, 0x00E, 0x04C, 0x088, 0x4C4, 0x880),
            (0xC04, 0x808, 0x40C, 0x00E, 0x04C, 0x088, 0x4C4, 0x880),
            (0xC04, 0x808, 0x000, 0x00E, 0x04C, 0x000, 0x4c4, 0x880),
            (0x000, 0x808, 0x40C, 0x00E, 0x04C, 0x088, 0x4C4, 0x000),
            (0x000, 0x000, 0x40C, 0x00E, 0x04C, 0x088, 0x000, 0x000),
        )
        initialize()
        set_image(image)
        set_pixel(12, 4, 0xFFF)

        while True:
            update()
            time.sleep(0.00001)

    except KeyboardInterrupt:
        print("\nGot tired... cleaning up and finishing!")
        turn_off()
