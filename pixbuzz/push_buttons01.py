import RPi.GPIO as GPIO
import time


RED = 29
YELLOW = 31
GREEN = 33
BLUE = 35
GPIO.setmode(GPIO.BOARD)
GPIO.setup((RED, YELLOW, GREEN, BLUE), GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

btns_state = {
    "red":    {"channel": RED,    "state": GPIO.input(RED)},
    "yellow": {"channel": YELLOW, "state": GPIO.input(YELLOW)},
    "green":  {"channel": GREEN,  "state": GPIO.input(GREEN)},
    "blue":   {"channel": BLUE,   "state": GPIO.input(BLUE)}
}


def button_event_detected(channel):
    for btn in btns_state.keys():
        if btns_state[btn]["channel"] == channel:
            # btns_state[btn]["state"] =  (1, 0)[btns_state[btn]["state"]]
            btns_state[btn]["state"] = GPIO.input(btns_state[btn]["channel"])


GPIO.add_event_detect(RED, GPIO.BOTH, callback=button_event_detected)
GPIO.add_event_detect(YELLOW, GPIO.BOTH, callback=button_event_detected)
GPIO.add_event_detect(GREEN, GPIO.BOTH, callback=button_event_detected)
GPIO.add_event_detect(BLUE, GPIO.BOTH, callback=button_event_detected)


try:
    while True:

        print("\nRED:", btns_state["red"]["state"])
        print("YELLOW:", btns_state["yellow"]["state"])
        print("GREEN:", btns_state["green"]["state"])
        print("BLUE:", btns_state["blue"]["state"])
        time.sleep(0.1)

except KeyboardInterrupt:
    print("\nGot tired... cleaning up and finishing!")
    GPIO.cleanup()
