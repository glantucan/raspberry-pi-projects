import buttons
import timing.timer as timer
import time
import pix.screen as screen

player_color = 0xAA0
pos = (1, 4)
frame_duration = 1/15
screen.initialize()
screen.set_pixel(pos[0], pos[1], player_color)
render_timer = timer.setInterval(0.00001, screen.update)


def move(directions):
    global pos
    screen.set_pixel(pos[0], pos[1], 0)
    for heading in directions:
        pos = {
            'left': lambda:  (pos[0] - 1 if pos[0] > 0 else 15, pos[1]),
            'right': lambda: (pos[0] + 1 if pos[0] < 15 else 0, pos[1]),
            'up': lambda:    (pos[0], pos[1] + 1 if pos[1] < 7 else 0),
            'down': lambda:  (pos[0], pos[1] - 1 if pos[1] > 0 else 7),
        }[heading]()
    screen.set_pixel(pos[0], pos[1], player_color)


try:
    while True:
        btns_state = buttons.get_state(1)
        move_dir = [btn for btn in btns_state.keys() if btns_state[btn]]
        move(move_dir)
        screen.update()
        time.sleep(frame_duration)

except KeyboardInterrupt:
    print("\nGot tired... cleaning up and finishing!")
    screen.turn_off()
    render_timer.cancel()

