'''Provides factory function to create button groups
'''
from collections import namedtuple
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

_ButtonsGroup = namedtuple('ButtonsGroup', ('name'))
_Button = namedtuple('Button', (
    'channel',
    'name',
    'callback',
    'activate',
    'deactivate'))


def _activate(button):
    button._active = True
    GPIO.add_event_detect(button.channel,
                          button.edge_detect,
                          button.callback)


def _deactivate(button):
    button._active = False
    GPIO.remove_event_detect(button.channel)


def edge_detected(channel):
    pass


def _create_button(btn_conf):
    return _Button(
        channel=btn_conf['channel'],
        name=btn_conf['name'],
        pressed=btn_conf['pressed'] if 'pressed' in btn_conf else False,
        activate=_activate,
        deactivate=_deactivate,
        callback=btn_conf['callback'] if 'callback' in btn_conf else None)


def create_group(group_name, btns_conf, pull_down=True,
                 edge_detect=GPIO.RISING):
    """ Creates a named tuple subclass contaning an inner tuple for each
        physical push buttons with convenient properties: channel, pressed

        Args:
            group_name (str)
            btns_conf (dictionary): dictionary containing
                channel (int)
                name (str)
                pressed (boolean, default: False)
                active (boolean)
                callback (function, optional)
    """

    Group = namedtuple('ButtonsGroup',
                       _ButtonsGroup._fields
                       + tuple([btn_conf['name'] for btn_conf in btns_conf]))

    # Instantiate the group named tuple and add button namedtuples to it
    group = Group(group_name,
                  *[_create_button(btn_conf) for btn_conf in btns_conf])

    GPIO.setup([btn_conf['channel'] for btn_conf in btns_conf],
               GPIO.IN,
               pull_up_down=(GPIO.PUD_DOWN, GPIO.PUD_UP)[pull_down])

    # Add event handlers if callback provided
    for btn_conf in btns_conf:

        if 'callback' in btn_conf:
            # The button will become active, i.e. respond to pushes unless
            # there is no calback or _active is set explicitly to false.
            if 'active' not in btn_conf:
                print(group)
                group.up.active = True

            if group[btn_conf['name']].active:
                GPIO.add_event_detect(btn_conf['channel'],
                                      edge_detect,
                                      btn_conf['callback'])

    return group


# For testing
if __name__ == '__main__':

    def whenPressed(channel='<unspecified>'):
        print('presed', channel)

    test_group = create_group('test', [
        {
            'name': 'up',
            'channel': 29
        },
        {
            'name': 'down',
            'channel': 33,
            'callback': whenPressed
        }
    ])

    print(test_group)
