from gpiozero import Button, ButtonBoard
from signal import pause
import time

P1_BTN_PINS = {
    'up': 5,  # 29
    'left': 6,  # 31
    'down': 13,  # 33
    'right': 19  # 35
}

p1_btn_actions = {
    'left': "Move left",
    'right': "Move right",
    'up': "Move up",
    'down': "Move down"
}
p1_state = {
    'left': False,
    'right': False,
    'up': False,
    'right': False
}


def move(board):
    global p1_state
    p1_state = {
        'left': board.left.value,
        'right': board.right.value,
        'up': board.up.value,
        'down': board.down.value
    }


def stop(pin):
    print('stop')


def started_moving_up():
    print('started movin gup')


# ButtonBoard.label = ''
p1_buttons = ButtonBoard(**P1_BTN_PINS)
# p1_buttons.label = 'p1_btns'
# print(p1_buttons.label)

p1_buttons.when_pressed = move
p1_buttons.when_released = move



frame = 1/60
while True:
    print(f'Moving {[btn for btn in p1_state.keys() if p1_state[btn]]}')
    time.sleep(frame)

