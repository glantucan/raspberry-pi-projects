
import sound_hw_buzzer as buzzer

version = '0.0.1'


def start(channels=None):
    buzzer.init(pins=channels)
    # buzzer._debug_print_pins()


def play(idx):
    pass


def shutdown():
    print("Stopping Sound Buzzer...")
    buzzer.shutdown()
