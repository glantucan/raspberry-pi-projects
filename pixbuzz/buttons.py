from gpiozero import ButtonBoard

P1_BTN_PINS = {
    'up': 5,  # 29
    'left': 6,  # 31
    'down': 13,  # 33
    'right': 19  # 35
}
_p1_buttons_state = {
    'left': False,
    'right': False,
    'up': False,
    'down': False
}


def _p1_state_change(board):
    global _p1_buttons_state
    _p1_buttons_state = {
        'left': board.left.value,
        'right': board.right.value,
        'up': board.up.value,
        'down': board.down.value
    }


_p1_buttons = ButtonBoard(**P1_BTN_PINS)
_p1_buttons.when_pressed = _p1_state_change
_p1_buttons.when_released = _p1_state_change


def get_state(player):
    return {
        1: _p1_buttons_state
    }[player]
