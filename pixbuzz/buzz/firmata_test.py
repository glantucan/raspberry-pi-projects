'''
    PWM arduino pins 3, 5, 6, 9, 10, 11
'''
from pymata4 import pymata4
import time
import sys

board = pymata4.Pymata4()
PIN1 = 3
PIN2 = 6
try:
    # set a pin's mode for tone operations
    board.set_pin_mode_tone(PIN1)
    board.set_pin_mode_tone(PIN2)

    # specify pin and frequency and play continuously
    for i in range(0, 500):
        board.play_tone_continuously(PIN1, 2000)
        time.sleep(0.001)
        board.play_tone_off(PIN1)
        board.play_tone_continuously(PIN2, 1000)
        time.sleep(0.001)
        board.play_tone_off(PIN2)
    # specify pin to turn pin off
    board.play_tone_off(PIN1)
    board.play_tone_off(PIN2)

    """
    # specify pin, frequency and duration and play tone
    board.play_tone(PIN1, 2000, 2000)
    time.sleep(0.5)
    board.play_tone(PIN1, 100, 500)
    time.sleep(5)
    """
except KeyboardInterrupt:
    print("\nGot tired... cleaning up and finishing!")
    board.shutdown()
    sys.exit(0)
