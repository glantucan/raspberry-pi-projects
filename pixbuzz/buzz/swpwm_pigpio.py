import pigpio
import time

"""[ 24.50, 27.50, 30.87],
    [32.70, 36.71, 41.20, 43.65, 49.00, 55.00, 61.74], 
    [65.41, 73.42, 82.41, 87.31, 98.00, 110.0, 123.47],
    [130.81, 146.83, 164.81, 174.61, 196.00, 220.00, 246.94]"""

C = 0, 
D = 1,
E = 2
F = 3
G = 4
A = 5
B = 6

notes = [
    [  33,   37,   41,   44,   49,   55,   62], 
    [  65,   73,   82,   87,   98,  110,  123],
    [ 131,  147,  165,  175,  196,  220,  247],
    [ 262,  294,  330,  349,  392,  440,  494],
    [ 523,  587,  659,  698,  784,  880,  988],
    [1047, 1175, 1319, 1397, 1568, 1760, 1976]
]
notes2  = [
    [  65,   73,   82,   87,   98,  110,  123],
    [ 131,  147,  165,  175,  196,  220,  247],
    [ 262,  294,  330,  349,  392,  440,  494],
    [ 523,  587,  659,  698,  784,  880,  988],
    [1047, 1175, 1319, 1397, 1568, 1760, 1976],
    [  33,   37,   41,   44,   49,   55,   62], 
]

notes3 = [
    [ 131,  147,  165,  175,  196,  220,  247],
    [ 262,  294,  330,  349,  392,  440,  494],
    [ 523,  587,  659,  698,  784,  880,  988],
    [1047, 1175, 1319, 1397, 1568, 1760, 1976],
    [  33,   37,   41,   44,   49,   55,   62], 
    [  65,   73,   82,   87,   98,  110,  123],
]

pi = pigpio.pi()

try:
    while True:
        for scale in range(0, len(notes)):
            for note in range(0, len(notes[scale])):
                
                pi.set_PWM_frequency(12, notes[scale][note])
                pi.set_PWM_dutycycle(12, 255)
                pi.set_PWM_frequency(13, notes2[scale][note])
                pi.set_PWM_dutycycle(13, 180)
                pi.set_PWM_frequency(18, notes3[scale][note])
                pi.set_PWM_dutycycle(18, 120)
                
                
                pi.hardware_PWM(12, 30, 0)
                pi.hardware_PWM(13, 30, 0)
                pi.hardware_PWM(18, 30, 0)
                    

        time.sleep(1)


except KeyboardInterrupt:
    print("\nGot tired... cleaning up and finishing!")
    pi.hardware_PWM(12, 30, 0)
    pi.hardware_PWM(13, 30, 0)
    pi.stop()

