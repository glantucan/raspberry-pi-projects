import pigpio
import time

"""[ 24.50, 27.50, 30.87],
    [32.70, 36.71, 41.20, 43.65, 49.00, 55.00, 61.74], 
    [65.41, 73.42, 82.41, 87.31, 98.00, 110.0, 123.47],
    [130.81, 146.83, 164.81, 174.61, 196.00, 220.00, 246.94]"""


notes = [
    #[  33,   37,   41,   44,   49,   55,   62], 
    #[  65,   73,   82,   87,   98,  110,  123],
    [ 131,  147,  165,  175,  196,  220,  247],
    [ 262,  294,  330,  349,  392,  440,  494],
    [ 523,  587,  659,  698,  784,  880,  988],
    [1047, 1175, 1319, 1397, 1568, 1760, 1976]
]
notes2  = [
    
    #[  65,   73,   82,   87,   98,  110,  123], 
    [ 262,  294,  330,  349,  392,  440,  494],
    [ 523,  587,  659,  698,  784,  880,  988],
    [1047, 1175, 1319, 1397, 1568, 1760, 1976],
    [ 131,  147,  165,  175,  196,  220,  247],
]

notes3 = [
    [ 523,  587,  659,  698,  784,  880,  988],
    #[  65,   73,   82,   87,   98,  110,  123],
    [1047, 1175, 1319, 1397, 1568, 1760, 1976], 
    [ 131,  147,  165,  175,  196,  220,  247],
    [ 262,  294,  330,  349,  392,  440,  494],
]

pi = pigpio.pi()
""" pi.hardware_PWM(12, 30, 0)
pi.hardware_PWM(12, 349, 500000) """
try:
    while True:
        for scale in range(0, len(notes)):
            for note in range(0, len(notes[scale])):
                for dc in range(0, 5):

                    pi.hardware_PWM(12, notes[scale][note], 600000)
                    time.sleep(0.015)
                    #pi.hardware_PWM(12, 30, 0)
                    pi.hardware_PWM(12, notes2[scale][note], 400000)
                    time.sleep(0.015)
                    #pi.hardware_PWM(12, 30, 0)
                    pi.hardware_PWM(12, notes3[scale][note], 200000)
                    time.sleep(0.015)
                    pi.hardware_PWM(12, 30, 0) 
                   

        time.sleep(1)
    pass

except KeyboardInterrupt:
    print("\nGot tired... cleaning up and finishing!")
    pi.hardware_PWM(12, 30, 0)
    pi.hardware_PWM(13, 30, 0)
    pi.stop()

