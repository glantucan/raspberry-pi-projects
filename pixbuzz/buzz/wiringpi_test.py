import wiringpi
import notes
import time

wiringpi.wiringPiSetupGpio()
PIN = 12
wiringpi.pinMode(PIN, 2) 
wiringpi.softToneCreate(PIN)
wiringpi.softToneWrite(PIN, notes.tone("5C"))

try:
    while True:
        time.sleep(1)


except KeyboardInterrupt:
    print("\nGot tired... cleaning up and finishing!")