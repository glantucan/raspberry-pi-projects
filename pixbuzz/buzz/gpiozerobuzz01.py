from gpiozero.pins.pigpio import PiGPIOPin
from gpiozero.pins.pigpio import PiGPIOFactory
from gpiozero import PWMOutputDevice
import time
import notes

factory = PiGPIOFactory()
scale = (
    "3C", "3D", "3E", "3F", "3G", "3A", "3B",
    "4C", "4D", "4E", "4F", "4G", "4A", "4B",
    "5C", "5D", "5E", "5F", "5G", "5A", "5B"
)

pwm1 = PWMOutputDevice(pin=12, pin_factory=factory)

for note in scale:
    pwm1.frequency = notes.tone(note)
    pwm1.value = 0.5
    time.sleep(1)
    pwm1.value = 0
    time.sleep(0.1)
pwm1.off()
