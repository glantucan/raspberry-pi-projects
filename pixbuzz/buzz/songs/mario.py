song = (
    "70,8",
    
    "5E,5E,-,5E,-,5C,5Ex2,  5Gx2,-,-,4Gx2,-,-,  5Cx2,-,4Gx2,-,4Ex2,  -,4Ax2,4Bx2,4Bb,4Ax2,  4G,5E,5G,5Ax2,5F,5G,  -,5Ex2,5C,5D,4Bx2,-,  -,-,5G,5F#,5F,5Ebx2,5E,  -,4Ab,4A,5C,-,4A,4C,4D,  -,-,5G,5F#,5F,5Ebx2,5E," +
    "  5F+5G+6Cx2,5F+5G+6C,5F+5G+6Cx2,-,- ,  -,-,5G,5F#,5F,5Ebx2,5E,  -,4Ab,4A,5C,-,4A,5C,5D,  -,-,5Ebx2,-,5Dx2,-,  4E+5Cx2,-,-,-,-,-,-",

    "3Dx8                ,  3Gx2,-,-,2Gx2,-,-,  3Gx2,-,3Ex2,-,3Cx2,  -,3Fx2,3Gx2,3F#,3Fx2,  3E,4C,4E,4Fx2,4D,4E,  -,4Cx2,3A,3B,3Gx2,-,  3C+3Gx8               ,  3F+4Cx8               ,  3C+3Gx8               ," +
    "  -,-,-,-,-                     ,3Gx2,  3Cx8                  ,  3Fx7,-                 ,  3Cx7,-           ,  4Cx7,-   " 
)

from functools import reduce
import notes
import pigpio
import time

bpm, sub = [int(val) for val in song[0].split(',')]
sub_time = 60/(bpm * sub)
sep_time = sub_time/8
arp_time = sub_time/8

def arpegiator(duration, chord):
    split_arp = chord.split('+')
    if len(split_arp) > 1:
        return (int(duration), *[notes.tone(note) for note in split_arp])
    else:
        return (int(duration), notes.tone(chord))



def notes_2_freq(note):
    """Retruns a tuple where:
        0    -> note duration in subdivisions
        1..n -> frequencies to arpegiate or to play if there's only one
    """
    if note == '-':
        return (1, 0)
    else:
        split_dur = note.split('x')
        if len(split_dur) == 1:
            return arpegiator(1, note)
        elif len(split_dur) == 2:
            return arpegiator(split_dur[1], split_dur[0])


voice1 = tuple([notes_2_freq(note) 
                for note in [note.strip() 
                             for note in song[1].split(',')]])
voice2 = tuple([notes_2_freq(note) 
                for note in [note.strip() 
                             for note in song[2].split(',')]])

print(voice1)
print(voice2)
print(len(voice1), reduce(lambda a, b: a + b, [note[0] for note in voice1]))
print(len(voice2), reduce(lambda a, b: a + b, [note[0] for note in voice2]))

# Play loop
pi = pigpio.pi()
voice1 = voice2
v1_sub_note_counter = 0
v2_sub_note_counter = 0
v1_note_duration = 0
cur_voice1 = ()
voice1_idx = 0
for sub in range(0, reduce(lambda a, b: a + b, [note[0] for note in voice1])):
    if v1_sub_note_counter == v1_note_duration:
        v1_sub_note_counter = 0
        v1_note_duration = voice1[voice1_idx][0]
        print(f"{sub}:", voice1[voice1_idx])
        print('  ', v1_sub_note_counter)
        time.sleep(sep_time)
        cur_voice1 = voice1[voice1_idx][1:]
        voice1_idx += 1

        if len(cur_voice1) > 1:
            for cur_note in cur_voice1:
                pi.hardware_PWM(12, cur_note, 500000)
                time.sleep(arp_time)
        else:
            pi.hardware_PWM(12, cur_voice1[0], 500000)
            time.sleep(sub_time)
    else:
        print('  ', v1_sub_note_counter)
        if len(cur_voice1) > 1:
            for cur_note in cur_voice1:
                pi.hardware_PWM(12, cur_note, 500000)
                time.sleep(arp_time)
        else:
            time.sleep(sub_time)
    v1_sub_note_counter += 1
