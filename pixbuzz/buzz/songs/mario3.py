song = (
    "90,8",
    
    "5E,5E,-,5E,-,5C,5Ex2,  5Gx2,-,-,4Gx2,-,-,  5Cx2,-,4Gx2,-,4Ex2,  -,4Ax2,4Bx2,4Bb,4Ax2,  4G,5E,5G,5Ax2,5F,5G,  -,5Ex2,5C,5D,4Bx2,-,  -,-,5G,5F#,5F,5Ebx2,5E,  -,4Ab,4A,5C,-,4A,4C,4D,  -,-,5G,5F#,5F,5Ebx2,5E," +
    "  5F+5G+6Cx2,5F+5G+6C,5F+5G+6Cx2,-,- ,  -,-,5G,5F#,5F,5Ebx2,5E,  -,4Ab,4A,5C,-,4A,5C,5D,  -,-,5Ebx2,-,5Dx2,-,  4E+5Cx2,-,-,-,-,-,-",

    "3Dx8                ,  3Gx2,-,-,2Gx2,-,-,  3Gx2,-,3Ex2,-,3Cx2,  -,3Fx2,3Gx2,3F#,3Fx2,  3E,4C,4E,4Fx2,4D,4E,  -,4Cx2,3A,3B,3Gx2,-,  3C+3Gx8               ,  3F+4Cx8               ,  3C+3Gx8               ," +
    "  -,-,-,-,-                     ,3Gx2,  3Cx8                  ,  3Fx7,-                 ,  3Cx7,-           ,  4Cx7,-   " 
)

from functools import reduce
import notes
import pigpio
import time
import timer

bpm, sub = [int(val) for val in song[0].split(',')]
sub_time = 60/(bpm * sub)
sep_time = sub_time/8
arp_time = sub_time/8

def notes_2_freq(duration, chord):
    split_arp = chord.split('+')
    if len(split_arp) > 1:
        return (int(duration), *[notes.tone(note) for note in split_arp])
    else:
        return (int(duration), notes.tone(chord))



def parse_chords(note):
    """Returns a tuple where:
        0    -> note duration in subdivisions
        1..n -> frequencies to arpegiate or to play if there's only one
    """
    if note == '-':
        return (1, 0)
    else:
        split_dur = note.split('x')
        if len(split_dur) == 1:
            return notes_2_freq(1, note)
        elif len(split_dur) == 2:
            return notes_2_freq(split_dur[1], split_dur[0])


voice1 = tuple([parse_chords(note) 
                for note in [note.strip() 
                             for note in song[1].split(',')]])
voice2 = tuple([parse_chords(note) 
                for note in [note.strip() 
                             for note in song[2].split(',')]])

print(voice1)
print(voice2)
print(len(voice1), reduce(lambda a, b: a + b, [note[0] for note in voice1]))
print(len(voice2), reduce(lambda a, b: a + b, [note[0] for note in voice2]))

# Play loop
pi = pigpio.pi()
v1_sub_note_counter = 0
v2_sub_note_counter = 0
v1_note_duration = 0
cur_voice1 = ()
voice1_idx = 0



import threading


""" class ArpegioPlayer:
    def __init__(self, arp, channel, duration, arpTime):
        self.arp = arp
        self.duration = duration
        self.arptime = arpTime
        self.pi = pigpio.pi()
        self.stopEvent = threading.Event()
        thread = threading.Thread(target=self.__playNextArp)
        thread.start()

    def __playNextArp(self):
        nextTime = time.time() + self.arpTime
        while not self.stopEvent.wait(nextTime - time.time()):
            
"""

class SongPlayer:
    def __init__(self, voices, bpm, subdivisions, loop=True):
        self.voices = voices
        self.bpm = bpm
        self.subdivisions = subdivisions  # subdivisions per beat
        self.currentlyPlaying = [None for voice in voices]
        self.subTime = (60/(self.bpm * self.subdivisions))
        self.sepTime = self.subTime / 8
        self.interval = self.sepTime  # Initially
        self.loop = loop
        self.stopEvent = threading.Event()
        self.pi = pigpio.pi()
        thread = threading.Thread(target=self.__playNext)
        thread.start()


    def __playNext(self):
        nextTime = time.time() + self.interval
        
        while not self.stopEvent.wait(nextTime - time.time()):
            voicesFinished = 0
            voiceIdx = 0
            # We alternate intervals to detect when a chord has player for its set duration and
            # be able to make a litle pause before playing the next one on that channel
            if self.interval == self.sepTime:
                self.interval = self.subTime - self.sepTime
                for voiceData in self.voices:
                    if not self.currentlyPlaying[voiceIdx]:
                        nextChord = next(voiceData.voice, False)
                        if (nextChord):
                            self.currentlyPlaying[voiceIdx] = nextChord
                            nextTime += self.interval
                            self.__playNote(voiceData.channel, nextChord[1:], voiceData.volume)
                        else:
                            print('finished ', voiceIdx)
                            voicesFinished += 1

                    else:
                        nextTime += self.interval

                    voiceIdx += 1
                    voiceData.durationCounter += 1

            else:
                self.interval = self.sepTime
                for voiceData in self.voices:
                    print(voiceIdx, self.currentlyPlaying[voiceIdx], self.currentlyPlaying[voiceIdx][0], voiceData.durationCounter)
                    if self.currentlyPlaying[voiceIdx][0] == voiceData.durationCounter:
                        voiceData.durationCounter = 0
                        self.currentlyPlaying[voiceIdx] = None
                        self.__playNote(voiceData.channel, [0], 0)
                        nextTime += self.interval

                    voiceIdx += 1

            if len(self.voices) == voicesFinished:
                voiceIdx = 0
                voicesFinished = 0
                if (self.loop):
                    for voice in self.voices:
                        voice.durationCounter = 0
                        voice.restart()
                        nextChord = next(voice.voice, False)
                        print(nextChord)
                        self.currentlyPlaying[voiceIdx] = nextChord
                        nextTime += self.interval
                        self.__playNote(voice.channel, nextChord[1:], voice.volume)
                        voiceIdx += 1
                        voice.durationCounter += 1
                else:
                    self.cancel()
            

    def __playNote(self, channel, chord, volume):
        self.pi.hardware_PWM(channel, chord[0], volume)
        # time.sleep()

    def cancel(self):
        print("\nGot tired... cleaning up and finishing!")
        pi.hardware_PWM(12, 30, 0)
        pi.hardware_PWM(13, 30, 0)
        pi.stop()
        self.stopEvent.set()


class VoiceData:
    def __init__(self, channel=0, voice=None, volume=500000, durationCounter=0):
        self.channel = channel
        self._voice = voice
        self.voice = iter(voice)
        self.volume = volume
        self.durationCounter = durationCounter
    
    def restart(self):
        print('Creating new iterator')
        self.voice = iter(self._voice)


voices = (
    VoiceData(channel=12,
              voice=voice1,
              volume=500000
    ),
    VoiceData(channel=13,
              voice=voice2,
              volume=500000
    )
)

try:
    player = SongPlayer(voices, bpm, sub)
except KeyboardInterrupt:
    player.cancel()
