song = (
    "70,8",
    
    "5E,5E,-,5E,-,5C,5Ex2,  5Gx2,-,-,4Gx2,-,-,  5Cx2,-,4Gx2,-,4Ex2,  -,4Ax2,4Bx2,4Bb,4Ax2,  4G,5E,5G,5Ax2,5F,5G,  -,5Ex2,5C,5D,4Bx2,-,  -,-,5G,5F#,5F,5Ebx2,5E,  -,4Ab,4A,5C,-,4A,4C,4D,  -,-,5G,5F#,5F,5Ebx2,5E," +
    "  5F+5G+6Cx2,5F+5G+6C,5F+5G+6Cx2,-,- ,  -,-,5G,5F#,5F,5Ebx2,5E,  -,4Ab,4A,5C,-,4A,5C,5D,  -,-,5Ebx2,-,5Dx2,-,  4E+5Cx2,-,-,-,-,-,-",

    "3Dx8                ,  3Gx2,-,-,2Gx2,-,-,  3Gx2,-,3Ex2,-,3Cx2,  -,3Fx2,3Gx2,3F#,3Fx2,  3E,4C,4E,4Fx2,4D,4E,  -,4Cx2,3A,3B,3Gx2,-,  3C+3Gx8               ,  3F+4Cx8               ,  3C+3Gx8               ," +
    "  -,-,-,-,-                     ,3Gx2,  3Cx8                  ,  3Fx7,-                 ,  3Cx7,-           ,  4Cx7,-   " 
)

from functools import reduce
import notes
import pigpio
import time
import timer

bpm, sub = [int(val) for val in song[0].split(',')]
sub_time = 60/(bpm * sub)
sep_time = sub_time/8
arp_time = sub_time/8

def arpegiator(duration, chord):
    split_arp = chord.split('+')
    if len(split_arp) > 1:
        return (int(duration), *[notes.tone(note) for note in split_arp])
    else:
        return (int(duration), notes.tone(chord))



def notes_2_freq(note):
    """Retruns a tuple where:
        0    -> note duration in subdivisions
        1..n -> frequencies to arpegiate or to play if there's only one
    """
    if note == '-':
        return (1, 0)
    else:
        split_dur = note.split('x')
        if len(split_dur) == 1:
            return arpegiator(1, note)
        elif len(split_dur) == 2:
            return arpegiator(split_dur[1], split_dur[0])


voice1 = tuple([notes_2_freq(note) 
                for note in [note.strip() 
                             for note in song[1].split(',')]])
voice2 = tuple([notes_2_freq(note) 
                for note in [note.strip() 
                             for note in song[2].split(',')]])

print(voice1)
print(voice2)
print(len(voice1), reduce(lambda a, b: a + b, [note[0] for note in voice1]))
print(len(voice2), reduce(lambda a, b: a + b, [note[0] for note in voice2]))

# Play loop
pi = pigpio.pi()
v1_sub_note_counter = 0
v2_sub_note_counter = 0
v1_note_duration = 0
cur_voice1 = ()
voice1_idx = 0



import threading


class VoicePlayer:
    def __init__(self, voice, channel, bpm, subdivisions, volume):
        self.voice = iter(voice)  # notes tuple of duration and frequencies tuples
        self.channel = channel  # pin channel number (hw PWM capable)
        self.bpm = bpm
        self.subdivisions = subdivisions  # subdivisions per beat
        self.volume = volume

        self.interval = (60/(self.bpm * self.subdivisions)) * voice[0][0]
        #print('interval:', self.interval)
        self.stopEvent = threading.Event()
        self.pi = pigpio.pi()
        thread = threading.Thread(target=self.__playNext)
        thread.start()


    def __playNext(self):
        nextTime = time.time() + self.interval
        while not self.stopEvent.wait(nextTime - time.time()):
            next_chord = next(self.voice, False)
            if (next_chord):
                self.interval = next_chord[0] * 60/(self.bpm * self.subdivisions)
                #print('interval:', self.interval)
                nextTime +=  self.interval
                self.__playNote(next_chord[1:])
            else:
                self.cancel()

    def __playNote(self, chord):
        self.pi.hardware_PWM(self.channel, chord[0], self.volume)
        #time.sleep()
        
    def cancel(self):
        self.stopEvent.set()


player2 = VoicePlayer(voice2, 13, bpm, sub, 500000)
player1 = VoicePlayer(voice1, 12, bpm, sub, 500000)

