import pigpio
import time
import notes

"""[ 24.50, 27.50, 30.87],
    [32.70, 36.71, 41.20, 43.65, 49.00, 55.00, 61.74], 
    [65.41, 73.42, 82.41, 87.31, 98.00, 110.0, 123.47],
    [130.81, 146.83, 164.81, 174.61, 196.00, 220.00, 246.94]"""

C = 0 
Cs = Df = 1
D = 2
Ds = Ef = 3
E = 4
F = 5
Fs = Gf = 6
G = 7
Gs = Af = 8
A = 9
As = Bf = 10
B = 6
# From C1 to A6

pi = pigpio.pi()
pi.hardware_clock(4, 500) # 5 KHz clock on GPIO 
time.sleep(1)
try:
    while True:
        for scale in range(0, len(notes)):
            for note in range(0, len(notes[scale])):
                for dc in range(0, 5):

                    pi.hardware_PWM(12, notes[scale][note], 600000)
                    time.sleep(0.04)
                    pi.hardware_PWM(12, 30, 0)
                    pi.hardware_PWM(12, notes2[scale][note], 400000)
                    time.sleep(0.04)
                    pi.hardware_PWM(12, 30, 0)
                    pi.hardware_PWM(12, notes3[scale][note], 200000)
                    time.sleep(0.04)
                    pi.hardware_PWM(12, 30, 0)
                    

        time.sleep(1)


except KeyboardInterrupt:
    print("\nGot tired... cleaning up and finishing!")
    pi.hardware_PWM(12, 30, 0)
    pi.hardware_PWM(13, 30, 0)
    pi.stop()

