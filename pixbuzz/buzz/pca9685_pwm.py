import board
import busio
import adafruit_pca9685
import time 
notes = [
    [ 24.50, 27.50, 30.87],
    [32.70, 36.71, 41.20, 43.65, 49.00, 55.00, 61.74],
    [65.41, 73.42, 82.41, 87.31, 98.00, 110.0, 123.47],
    [130.81, 146.83, 164.81, 174.61, 196.00, 220.00, 246.94]
]
i2c = busio.I2C(board.SCL, board.SDA)
pca = adafruit_pca9685.PCA9685(i2c)

pca.frequency = 500

buzz_channel = pca.channels[8]

buzz_channel.duty_cycle = 0x5000

for o in notes:
    for n in o:
        pca.frequency = n
        print(n)
        time.sleep(.5)
buzz_channel.duty_cycle = 0x0000

# SHIT! Frequency cannot be set independentlyon different channels 
pca.deinit()