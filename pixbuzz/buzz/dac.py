import board
import busio
import adafruit_mcp4725
import time

i2c = busio.I2C(board.SCL, board.SDA)
dac = adafruit_mcp4725.MCP4725(i2c, address=0x60)


for i in range(0,60000):
    dac.normalized_value = 1.0
    time.sleep(0.001)
    dac.normalized_value = 0.0