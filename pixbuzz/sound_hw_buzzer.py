import RPi.GPIO as GPIO
import time

_pins = ()


def _play_test():
    for idx, pin in enumerate(_pins):
        print(idx, 300 + 500*idx, 'Hz')
        GPIO.output(pin, True)
        pwm = GPIO.PWM(pin, 300 + 500*idx)
        pwm.start(30)
        time.sleep(.1)
        pwm.stop()


def init(pins):
    global _pins
    _pins = pins

    # Clean up GPIO just in case before starting
    # GPIO.cleanup()
    GPIO.setmode(GPIO.BOARD)
    # GPIO.setwarnings(False)
    GPIO.setup(_pins, GPIO.OUT)
    _play_test()


def _debug_print_pins():
    print('Sound channels (BOARD pins) taken:')
    for idx, pin in enumerate(_pins):
        print(f"  {idx}: {pin}")


def shutdown():
    print('Liberating GPIO pins...')
    GPIO.cleanup()
    print('... done.')
