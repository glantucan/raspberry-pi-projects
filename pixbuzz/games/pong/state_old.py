MOVE_UP = 'up'
MOVE_DOWN = 'down'

_pong = {
    'player1': {
        'is_moving': False,
        'size': 2,
        'position': (14, 6),
        'move_delay': 2,
        'color': 0xAAA
    },
    'player2': {
        'is_moving': False,
        'size': 2,
        'position': (1, 3),
        'move_delay': 2,
        'color': 0xAAA
    },
    'ball': {
        'position': (8, 3),
        'move_delay': 1,
        'direction': 45,  # Use angles for the direction for easy math
                          # 0, 45, 135, 180, 225, 270, 315 allowed only
        'color': 0xA0A
    },
}

_player1_updates = None
_player2_updates = None
_ball_updates = None


def clamp(val, low, high):
    return max(low, min(high, val))


def _set_position(go_name, x=None, y=None):
    global _pong
    go = _pong[go_name]
    prev_pos = go['position']
    go['position'] = (
        x if x is not None else prev_pos[0],
        y if y is not None else prev_pos[1]
    )


def _player_update(player_name):
    direction = _pong[player_name]['is_moving']
    print('_player_update', direction)
    for frame in range(0, _pong[player_name]['move_delay']):
        if _pong[player_name]['is_moving']:
            yield
        else:
            return
    change = 1 if direction == MOVE_UP else -1
    _set_position(player_name, None,
                  clamp(_pong['player_name']['position'][1] + change, 0, 15))
    _pong[player_name]['is_moving'] = False
    return


def _ball_update():
    for frame in range(0, _pong['ball']['move_delay']):
        print('wait for ball update')
        yield
    print('updating')
    
    prev_position = _pong['ball']['position']
    prev_angle = _pong['ball']['direction']
    angle = prev_angle
    p1_pos = _pong['player1']['position']
    p1_size = _pong['player1']['size']
    p2_pos = _pong['player2']['position']
    p2_size = _pong['player2']['size']

    # Walls collision
    if angle != 0 and angle != 180:
        if prev_position[1] == 7:
            if angle == 45:
                angle = 315
            elif angle == 135:
                angle = 225
        elif prev_position[1] == 0:
            if angle == 315:
                angle = 45
            elif angle == 225:
                angle = 135

    # Player1 collision
    if (prev_position[0] == p1_pos[0] - 1 and
            prev_position[1] in range(p1_pos[1], p1_pos[1] + p1_size)):
        print('BOUNCE p1')
        angle = {
            0: 180,
            45: 135,
            315: 225
        }[angle]
        print(prev_angle, angle)
        if _pong['player1']['is_moving']:
            if _pong['player1']['is_moving'] == MOVE_DOWN:
                angle = clamp(angle + 45, 135, 225)
            else:
                angle = clamp(angle - 45, 135, 225)
    # Player2 collision
    if (prev_position[0] == p2_pos[0] + 1 and
            prev_position[1] in range(p2_pos[1], p2_pos[1] + p2_size)):
        angle = {
            180: 0,
            135: 45,
            225: 315
        }[angle]

        if _pong['player2']['is_moving']:
            if _pong['player2']['is_moving'] == MOVE_DOWN:
                angle = clamp(angle - 45, 135, 225)
            else:
                angle = clamp(angle + 45, 135, 225)

    _pong['ball']['direction'] = angle
    # Compute new position
    _pong['ball']['position'] = {
        0:   (prev_position[0] + 1, prev_position[1]),
        45:  (prev_position[0] + 1, prev_position[1] + 1),
        135: (prev_position[0] - 1, prev_position[1] + 1),
        180: (prev_position[0] - 1, prev_position[1]),
        225: (prev_position[0] - 1, prev_position[1] - 1),
        315: (prev_position[0] + 1, prev_position[1] - 1)
    }[angle]
    global _ball_updates
    _ball_updates = None
    yield


def update():
    """Must be executed after controller input has been processed
    """
    global _ball_updates
    global _player1_updates
    print('---')
    if _ball_updates:
        next(_ball_updates)
    else:
        _ball_updates = _ball_update()
        next(_ball_updates)

    """ if _player1_updates:
        next(_player1_updates) """


def move_player(player_name, direction):
    global _player1_updates
    _pong[player_name]['is_moving'] = direction
    if not _player1_updates:
        _player1_updates = _player_update('player1')


def get_player(player_name):
    position = _pong[player_name]['position']
    return {
        'position': [(position[0], position[1] + player_part)
                     for player_part in range(0, _pong[player_name]['size'])],
        'color': _pong[player_name]['color']
    }


def get_ball():
    return {
        'position': _pong['ball']['position'],
        'color': _pong['ball']['color']
    }
