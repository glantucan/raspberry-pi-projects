MOVE_UP = 'up'
MOVE_DOWN = 'down'

_pong = {
    'fps': 20,
    'time_step': 1/20,
    'width': 15,
    'height': 7,
    'player1': {
        'is_moving': False,
        'size': 2,
        'position': (14, 6),
        'move_delay': 3,
        'color': 0xAAA,
        'active': True
    },
    'player2': {
        'is_moving': False,
        'size': 2,
        'position': (1, 3),
        'move_delay': 3,
        'color': 0xAAA,
        'active': False
    },
    'ball': {
        'position': (8, 3),
        'direction': (1, 1),
        'move_delay': 2,
        'color': 0xA0A
    },
}

_player1_updates = None
_player2_updates = None
_ball_updates = None


def _clamp(val, low, high):
    print(val, low, high,  min(high, val))
    return max(low, min(high, val))


def _set_position(go_name, x=None, y=None):
    global _pong
    go = _pong[go_name]
    prev_pos = go['position']
    go['position'] = (
        x if x is not None else prev_pos[0],
        y if y is not None else prev_pos[1]
    )


def _player_update(player_name):

    for frame in range(0, _pong[player_name]['move_delay']):
        yield

    direction = _pong[player_name]['is_moving']
    change = 1 if direction == MOVE_UP else -1
    _set_position(player_name, None,
                  _clamp(_pong[player_name]['position'][1] + change,
                         0,
                         _pong['height'] - _pong[player_name]['size'] + 1))

    _pong[player_name]['is_moving'] = False

    if player_name == 'player1':
        global _player1_updates
        _player1_updates = None
    if player_name == 'player2':
        global _player2_updates
        _player2_updates = None
    yield


def _ball_update():
    for frame in range(0, _pong['ball']['move_delay']):
        # print('wait for ball update')
        yield

    bx, by = _pong['ball']['position']
    v0 = _pong['ball']['direction']
    v0x, v0y = v0
    vx = v0x
    vy = v0y
    print('pos:', (bx, by), 'v0:', v0, 'UPDATING:')

    p1x, p1y = _pong['player1']['position']
    p1size = _pong['player1']['size']
    p2x, p2y = _pong['player2']['position']
    p2size = _pong['player2']['size']

    # Walls collision
    if v0 != (1, 0) and v0 != (-1, 0):
        if by == _pong['height'] or by == 0:
            vy = -v0y

    # Player1 collision
    if _pong['player1']['active']:
        if (bx == p1x - 1 and vx > 0 and by in range(p1y, p1y + p1size)):
            print('BOUNCE player1')
            vx = -v0x
            print((v0x, v0y), (vx, vy))
            if _pong['player1']['is_moving'] == MOVE_DOWN:
                vy = max(-1, vy - 1)
                print('Player1 was moving down ->', (vx, vy))
            elif _pong['player1']['is_moving'] == MOVE_UP:
                vy = min(1, vy + 1)
                print('Player1 was moving up ->', (vx, vy))
            if _pong['fps'] < 120:
                _pong['fps'] += 4
                _pong['time_step'] = 1 / _pong['fps']

    # Player2 collision
    if _pong['player2']['active']:
        if (bx == p2x - 1 and vx < 0 and by in range(p2y, p2y + p2size)):
            print('BOUNCE player2')
            vx = -v0x
            print((v0x, v0y), (vx, vy))
            if _pong['player2']['is_moving'] == MOVE_DOWN:
                vy = max(-1, vy - 1)
                print('Player2 was moving down ->', (vx, vy))
            elif _pong['player2']['is_moving'] == MOVE_UP:
                vy = min(1, vy + 1)
                print('Player2 was moving up ->', (vx, vy))

    # Left GOAL collision
    if bx == 0:
        vx = -vx
        print('LEFT goal collision')
    # Right GOAL collision
    if bx == _pong['width']:
        vx = -vx
        print('RIGHT goal collision')
    _pong['ball']['direction'] = (vx, vy)
    # Compute new position
    _pong['ball']['position'] = (bx + vx, by + vy)

    print('new position:', (bx + vx, by + vy))
    global _ball_updates
    _ball_updates = None
    yield


def update():
    """Must be executed after controller input has been processed
    """
    global _ball_updates
    global _player1_updates
    print('---')
    if _ball_updates:
        next(_ball_updates)
    else:
        _ball_updates = _ball_update()
        next(_ball_updates)

    if _pong['player1']['is_moving']:
        if _player1_updates:
            next(_player1_updates)
        else:
            _player1_updates = _player_update('player1')
            next(_player1_updates)


def move_player(player_name, direction):
    global _player1_updates
    _pong[player_name]['is_moving'] = direction
    if not _player1_updates:
        _player1_updates = _player_update('player1')


def get_player(player_name):
    position = _pong[player_name]['position']
    return {
        'position': [(position[0], position[1] + player_part)
                     for player_part in range(0, _pong[player_name]['size'])],
        'color': _pong[player_name]['color']
    }


def get_ball():
    return {
        'position': _pong['ball']['position'],
        'color': _pong['ball']['color']
    }


def get_time_step():
    return _pong['time_step']
