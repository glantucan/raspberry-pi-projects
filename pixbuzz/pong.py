import pix.screen as screen
import timing.timer as timer
import time
import buttons
import games.pong.state as pong_state


screen.initialize()
render_timer = timer.setInterval(0.00001, screen.update)
try:
    while True:
        # Process buttons input
        btns_state = buttons.get_state(1)
        player_pressed = [btn for btn in btns_state.keys() if btns_state[btn]]

        player_move = [move for move in player_pressed
                       if move == pong_state.MOVE_UP
                       or move == pong_state.MOVE_DOWN]
        if len(player_move) == 1:
            pong_state.move_player('player1', player_move[0])

        # Update state
        pong_state.update()

        # Clear screen
        screen.clear()
        # render player1 state
        p1_state = pong_state.get_player('player1')
        for p1_pix in p1_state['position']:
            screen.set_pixel(*p1_pix, p1_state['color'])
        # render ball
        ball_state = pong_state.get_ball()
        screen.set_pixel(*ball_state['position'], ball_state['color'])
        # print(pong_state.get_player('player1'))

        time.sleep(pong_state.get_time_step())

except KeyboardInterrupt:
    print("\nGot tired... cleaning up and finishing!")
    screen.turn_off()
    render_timer.cancel()
