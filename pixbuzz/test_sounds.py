"""
For now a stand alone script, but this should end up being the game engine
used by all games for this kind of hardware:
    - five passive buzzers for sound
    - two 8x8 LED matrices arrays as screen
    - 4x2 arcade buttons for 2 player controllers
    - Game selector and display ???
"""

import time
import sound_buzzer as sound_player

sound_channels = (29, 31, 33, 35, 37)

conf = {
    "sound": {
        "music": {
            "fpn": 5,
            "song": [
                {
                    "repeat": 0,
                    "seq": [
                        ["kick",  "", "", "", ""],
                        ["",      "", "", "", ""],
                        ["",      "", "", "", ""],
                        ["",      "", "", "", ""],
                        ["snare", "", "", "", ""],
                        ["",      "", "", "", ""],
                        ["",      "", "", "", ""],
                        ["",      "", "", "", ""]
                    ]
                },
                {
                    "repeat": 16,
                    "seq": [
                        ["", "1F#", "", "", ""],
                        ["", "1F#", "", "", ""],
                        ["", "1F#", "", "", ""],
                        ["", "1F#", "", "", ""],
                        ["", "1F#", "", "", ""],
                        ["", "1F#", "", "", ""],
                        ["", "1F#", "", "", ""],
                        ["", "1F#", "", "", ""]
                    ]
                },
                {
                    "repeat": 4,
                    "seq": [
                        ["", "2F#", "", "", ""],
                        ["", "2F#", "", "", ""],
                        ["", "2F#", "", "", ""],
                        ["", "2F#", "", "", ""],
                        ["", "2F#", "", "", ""],
                        ["", "2F#", "", "", ""],
                        ["", "2F#", "", "", ""],
                        ["", "2F#", "", "", ""]
                    ]
                },
                {
                    "repeat": 4,
                    "seq": [
                        ["", "2C#", "", "", ""],
                        ["", "2C#", "", "", ""],
                        ["", "2C#", "", "", ""],
                        ["", "2C#", "", "", ""],
                        ["", "2C#", "", "", ""],
                        ["", "2C#", "", "", ""],
                        ["", "2C#", "", "", ""],
                        ["", "2C#", "", "", ""]
                    ]
                },
                {
                    "repeat": 4,
                    "seq": [
                        ["", "2E", "", "", ""],
                        ["", "2E", "", "", ""],
                        ["", "2E", "", "", ""],
                        ["", "2E", "", "", ""],
                        ["", "2E", "", "", ""],
                        ["", "2E", "", "", ""],
                        ["", "2E", "", "", ""],
                        ["", "2E", "", "", ""]
                    ]
                },
                {
                    "repeat": 4,
                    "seq": [
                        ["", "1B", "", "", ""],
                        ["", "1B", "", "", ""],
                        ["", "1B", "", "", ""],
                        ["", "1B", "", "", ""],
                        ["", "1B", "", "", ""],
                        ["", "1B", "", "", ""],
                        ["", "1B", "", "", ""],
                        ["", "1B", "", "", ""]
                    ]
                }
            ]
        }
    }
}


def start_loop(fps=60):
    delta_target = 1/fps
    prev_time = time.time()

    while True:

        # Update data
        cur_sound = 0
        # play sound
        sound_player.play(cur_sound)

        # update screen
        # print(delta_target, delta, delta_target - delta)

        cur_time = time.time()
        delta = cur_time - prev_time

        time.sleep(max(delta_target - delta, 0))
        prev_time = time.time()


if __name__ == '__main__':
    try:
        print(f'Loading Sound Buzzer version {sound_player.version}')
        sound_player.start(channels=sound_channels)
        start_loop(fps=40)

    except KeyboardInterrupt:
        print("\nGot tired... cleaning up and finishing!")
        sound_player.shutdown()
