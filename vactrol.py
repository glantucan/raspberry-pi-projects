import pigpio
import time

pi = pigpio.pi()

pin = 26

wait = 1

# By default pwm can be 10, 20, 40, 50, 80, 100, 160, 200, 250, 320, 
# 400, 500, 800, 1000, 1600, 2000, 4000 8000 
pi.set_PWM_frequency(pin, 20000)
pi.set_PWM_range(pin, 1000)

pi.set_PWM_dutycycle(pin, 990)
print('dc =', 990)
time.sleep(wait)

pi.set_PWM_dutycycle(pin, 950)
print('dc =', 950)
time.sleep(wait)

pi.set_PWM_dutycycle(pin, 850)
print('dc =', 850)
time.sleep(wait)

pi.set_PWM_dutycycle(pin, 750)
print('dc =', 750)
time.sleep(wait)

pi.set_PWM_dutycycle(pin, 500)
print('dc =', 500)
time.sleep(wait)

pi.set_PWM_dutycycle(pin, 250)
print('dc =', 250)
time.sleep(wait)

pi.set_PWM_dutycycle(pin, 100)
print('dc =', 100)
time.sleep(wait)

pi.set_PWM_dutycycle(pin, 50)
print('dc =', 50)
time.sleep(wait)

pi.set_PWM_dutycycle(pin, 25)
print('dc =', 25)
time.sleep(wait)


pi.set_PWM_dutycycle(pin, 24)
print('dc =', 25)
time.sleep(wait)


pi.set_PWM_dutycycle(pin, 10)
print('dc =', 25)
time.sleep(wait)


pi.set_PWM_dutycycle(pin, 5)
print('dc =', 25)
time.sleep(wait)

print('dc =', 0)
pi.set_PWM_dutycycle(pin, 0)
