import RPi.GPIO as GPIO
import time


RED = 18
GREEN = 22
YELLOW = 24
BLUE = 26

RED_BTN = 32
GREEN_BTN = 36
YELLOW_BTN = 38
BLUE_BTN = 40

def hw_setup():
    GPIO.setmode(GPIO.BOARD)
    GPIO.setwarnings(False)
    
    GPIO.setup(RED, GPIO.OUT)
    

def led_on(color):
    GPIO.output(color, GPIO.HIGH)
    
    
def led_off(color):
	GPIO.output(color, GPIO.LOW)
    

hw_setup()

try:
    while True:
        led_on(RED)
        time.sleep(1)
        led_off(RED)
        time.sleep(1)

except KeyboardInterrupt:
    print("\nGot tired... cleaning up and finishing!")
    GPIO.cleanup()

