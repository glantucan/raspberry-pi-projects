import time


prev_time = time.time()
acc_time = 0
delta = 1/60

while acc_time < 10.0:
    new_time = time.time()
    acc_time += new_time - prev_time
    print(f"acc: {acc_time}, delta: {new_time - prev_time}")
    prev_time = new_time
    time.sleep(delta - (time.time() - prev_time))
